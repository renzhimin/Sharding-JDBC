package com.taikang.Mapper;

import com.taikang.model.Aa;
import com.taikang.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper {


    @Select("select * from aa where id = #{id}")
    Aa getAa(String id);

    @Insert("INSERT INTO `aa`(`id`, `title`, `name`, `option`) VALUES (#{id}, #{title}, #{name}, #{option})")
    void addAd(Aa aa);

    @Insert("insert into user(id,username,password) values (#{id},#{username},#{password})")
    void addUser(User user);

    @Select("select * from user where id = #{id}")
    User getUser(Integer id);

    @Select("select * from user limit #{nums}")
    List<User> getUserList(int nums);
}
