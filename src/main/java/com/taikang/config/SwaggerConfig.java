package com.taikang.config;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author itw_xiekang
 */
@Configuration
//@ConditionalOnProperty(prefix = "swagger2", value = "enable", havingValue = "true")
@EnableSwagger2
public class SwaggerConfig {

//    @Value("${swagger2.enable}")
//    private boolean swagger2Enable;

    private static  final  String splitor=",";

    @Bean
    public Docket createRestApi() {
        ParameterBuilder parameterBuilder = new ParameterBuilder();
        List<Parameter> parameterList = new ArrayList<>();
        parameterBuilder.name("version").description("api version")
                .scalarExample("v1")
                .modelRef(new ModelRef("string")).parameterType("path")
                .required(true).build();
        parameterList.add(parameterBuilder.build());

        return new Docket(DocumentationType.SWAGGER_2)
                .globalOperationParameters(parameterList)
                .apiInfo(apiInfo())
                .enable(true)
                .select()
                .apis(basePackage("com.taikang.controller"))
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(false);
    }

    private static Optional<? extends Class<?>> declaringClass(RequestHandler input) {
        return Optional.fromNullable(input.declaringClass());
    }

    private static Function<Class<?>, Boolean> handlerPackage(final String basePackage) {
        return input ->{
            if(basePackage.contains(splitor)){
                for (String s : basePackage.split(splitor)) {
                    boolean isMatch = input.getPackage().getName().startsWith(s);
                    if (isMatch) return true;
                }
            }else {
                boolean isMatch = input.getPackage().getName().startsWith(basePackage);
                if (isMatch) return true;
            }
            return  false;
        };
    }
    public static Predicate<RequestHandler> basePackage(final String basePackage) {
       return input->declaringClass(input).transform(handlerPackage(basePackage)).or(true);
    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("HCloudTraining 接口文档")
                .description("接口文档开发版本")
                .termsOfServiceUrl("http://localhost:8080/swagger-ui.html")
                .version("1.0")
                .build();
    }
}



