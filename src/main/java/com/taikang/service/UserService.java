package com.taikang.service;


import com.taikang.Mapper.UserMapper;
import com.taikang.model.Aa;
import com.taikang.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserMapper userMapper;

    public Aa getAa(String id){

        return userMapper.getAa(id);
    }

    public void addAa(Aa aa){
        userMapper.addAd(aa);
    }

    public void addUser(User user) {
        userMapper.addUser(user);
    }

    public User getUser(Integer id) {
        return userMapper.getUser(id);
    }

    public List<User> getUserList(int nums) {

        return userMapper.getUserList(nums);

    }
}
