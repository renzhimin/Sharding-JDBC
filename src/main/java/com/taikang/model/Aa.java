package com.taikang.model;


public class Aa {

    /**
     * CREATE TABLE `aa` (
     `id` char(255) NOT NULL,
     `title` varchar(255) NOT NULL,
     `name` varchar(255) NOT NULL,
     `option` varchar(255) NOT NULL
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
     */

    private String id;

    private String title;

    private String name;

    private String option;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
}
