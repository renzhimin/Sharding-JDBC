package com.taikang.controller;

import com.taikang.model.Aa;
import com.taikang.model.User;
import com.taikang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {


    @Autowired
    UserService userService;


    @PostMapping(value = "addAa")
    public String addAa(@RequestBody Aa aa){
        userService.addAa(aa);
        return "success";
    }

    @GetMapping(value = "getAa")
    public Aa getAa(String id){
        return userService.getAa(id);
    }


    @PostMapping(value = "addUser")
    public String addUser(@RequestBody User user){
        for (int i=0;i<1000;i++){
            user = new User();
            user.setId(i);
            user.setPassword(String.valueOf(i));
            user.setUsername(String.valueOf(i));
            userService.addUser(user);
        }

        return "success";
    }

    @GetMapping(value = "getUser")
    public User getUser(Integer id){
        User user = userService.getUser(id);
        return user;
    }


    @GetMapping(value = "getUserList")
    public List<User> getUserList(int nums){
        return userService.getUserList(nums);
    }
}
