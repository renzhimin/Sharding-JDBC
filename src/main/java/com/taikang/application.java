package com.taikang;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@MapperScan("com.taikang.Mapper")
public class application {

    public static void main(String[] args) {
        SpringApplication.run(application.class,args);
    }

}
